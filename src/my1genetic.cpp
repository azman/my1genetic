//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef DO_MINGW
#include <conio.h>
#else
#include <unistd.h> // STDIN_FILENO
#include <termios.h> // struct termios
//------------------------------------------------------------------------------
#define MASK_LFLAG (ICANON|ECHO|ECHOE|ISIG)
//------------------------------------------------------------------------------
int getch(void)
{
	struct termios oldt, newt;
	int ch;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~MASK_LFLAG;
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	return ch;
}
#endif
//------------------------------------------------------------------------------
#include "my1gene.hpp"
//------------------------------------------------------------------------------
// START LIST - ERRORCODES
#define ERROR_GEN_NOPOP -10
#define ERROR_GEN_ERROR -11
#define ERROR_EXT_EVO_INVALID -20
#define ERROR_EXT_EVAL_ACTIVE -21 // NOT USED ANY LONGER!!!
#define ERROR_EXT_EVO_MUST -22
#define ERROR_EXT_W_VECFILE -23
#define ERROR_EXT_W_GENFILE -24
#define ERROR_UPD_EVO_INVALID -30
#define ERROR_UPD_EVAL_INACTIVE -31
#define ERROR_UPD_R_FITFILE -32
#define ERROR_UPD_W_GENFILE -33
#define ERROR_EVO_EVAL_ACTIVE -40 // NOT USED ANY LONGER!!!
#define ERROR_EVO_NOT_READY -41
#define ERROR_EVO_EXEC_ERROR -42
#define ERROR_EVO_W_GENFILE -43
// END LIST - ERRORCODES
#define OPTION_NOCMD 0x0
#define OPTION_HELPU 0x1
#define OPTION_SHOWF 0x2
#define OPTION_EVCHK 0x3
#define OPTION_EVRST 0x4
#define OPTION_GOEVO 0x5
#define OPTION_D8FIT 0x6
#define OPTION_X2VEC 0x7
#define OPTION_X2ARR 0x8
#define OPTION_GVIEW 0x9

#define FILE_ID_STRLEN 20
#define C_ARRAY_DATA_PER_LINE 10
//------------------------------------------------------------------------------
const static char GEN_FILENAME[]="gen_pop.txt";
const static char VEC_FILENAME[]="vec_arr.txt";
const static char FIT_FILENAME[]="fit_val.txt";
const static char ARR_FILENAME[]="net_array.h";
//------------------------------------------------------------------------------
char LowerCase(char aChar)
{
	char cTest = aChar;
	if(aChar>=0x41&&aChar<=0x5a)
		cTest += 0x20; // make lowercase
	return cTest;
}
//------------------------------------------------------------------------------
void WaitKey(void)
{
	printf("Press any key to continue...");
	getch();
	printf("\r                            \r");
}
//------------------------------------------------------------------------------
struct my1GenePoolInfo
{
	my1GeneInfo mGeneInfo;
	int mGeneCount, mGenomeCount;
};
//------------------------------------------------------------------------------
int CreatePopulation(my1GenePool* aPopulation, my1GenePoolInfo* anInfo)
{
	my1GeneInfo *pInfo = aPopulation->GetGeneInfo();
	*pInfo = anInfo->mGeneInfo;
	my1Gene* cGene = 0x0;
	switch(pInfo->mType)
	{
		default: pInfo->mType = GENE_INTEGER;
		case GENE_INTEGER:
		case GENE_FLOAT:
		{
			cGene = new my1Gene(pInfo); // assume okay
			cGene->Random();
		}
	}
	int cGenomeCount = anInfo->mGenomeCount;
	int cGeneCount = anInfo->mGeneCount;
	for(int cLoop=0;cLoop<cGenomeCount;cLoop++)
	{
		my1Genome* cGenome  = new my1Genome;
		for(int cLoop1=0;cLoop1<cGeneCount;cLoop1++)
		{
			cGene->Random();
			cGenome->InsertGene(cLoop1,*cGene); // insert last index
		}
		cGenome->SetFitness(IMPOSSIBLY_UNFIT);
		aPopulation->InsertGenome(cLoop,cGenome); // insert last
		delete cGenome;
	}
	delete cGene;
	return aPopulation->PopulationSize();
}
//------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	my1GenePool cPopulation;
	char cFileName[80], cFileSEC[80];
	int cGotCmd = OPTION_NOCMD;
	int temp, test = 0, index = -1, debug = 1;
	int backval = 0, errcmd = 0, served = 0;

	// set default gen_file name
	strcpy(cFileName,GEN_FILENAME);
	cFileSEC[0] = 0x0;

	if(argc>1)
	{
		// loop through argument, find parameters
		for(int cLoop=1;cLoop<argc;cLoop++)
		{
			//printf("Argument[%d] = %s, Length = %d\n",
			//    cLoop,argv[cLoop],strlen(argv[cLoop]));
			temp = strlen(argv[cLoop]);
			if(temp>1&&(argv[cLoop][0]=='-')) // an option
			{
				if(argv[cLoop][1]=='f') // give genetic filename
				{
					if(temp>2)
					{
						strcpy(cFileName,&argv[cLoop][2]);
						cFileName[temp-1] = 0x0;
					}
				}
				else if(argv[cLoop][1]=='i') // give evolution index
				{
					if(temp>2)
						index = atoi(&argv[cLoop][2]);
				}
				else if(argv[cLoop][1]=='x') // extract chromo to vector file
				{
					if(cGotCmd<OPTION_X2VEC)
					{
						cGotCmd = OPTION_X2VEC;
						if(temp>2)
						{   // specify vector filename
							strcpy(cFileSEC,&argv[cLoop][2]);
							cFileSEC[temp-1] = 0x0;
						}
						else
						{
							strcpy(cFileSEC,VEC_FILENAME);
						}
					}
				}
				else if(argv[cLoop][1]=='a') // SPECIAL! => extract chromo to c array!
				{
					if(cGotCmd<OPTION_X2ARR)
					{
						cGotCmd = OPTION_X2ARR;
						if(temp>2)
						{   // specify vector filename
							strcpy(cFileSEC,&argv[cLoop][2]);
							cFileSEC[temp-1] = 0x0;
						}
						else
						{
							strcpy(cFileSEC,ARR_FILENAME);
						}
					}
				}
				else if(argv[cLoop][1]=='p') // print population info
				{
					if(temp>2) errcmd = 1;
					else if(cGotCmd<OPTION_GVIEW)
						cGotCmd = OPTION_GVIEW;
				}
				else if(argv[cLoop][1]=='u') // update chromo from fitness file
				{
					if(cGotCmd<OPTION_D8FIT)
					{
						cGotCmd = OPTION_D8FIT;
						if(temp>2)
						{   // specify fitness filename
							strcpy(cFileSEC,&argv[cLoop][2]);
							cFileSEC[temp-1] = 0x0;
						}
						else
							strcpy(cFileSEC,FIT_FILENAME);
					}
				}
				else if(argv[cLoop][1]=='e') // evolution!
				{
					if(cGotCmd<OPTION_GOEVO)
					{
						cGotCmd = OPTION_GOEVO;
						if(temp>2)
						{   // specify population fitness log filename
							strcpy(cFileSEC,&argv[cLoop][2]);
							cFileSEC[temp-1] = 0x0;
						}
						else
							cFileSEC[0] = 0x0;
					}
				}
				else if(argv[cLoop][1]=='r') // reset evolution status!
				{
					if(temp>2) errcmd = 1;
					else if(cGotCmd<OPTION_EVRST)
						cGotCmd = OPTION_EVRST;
				}
				else if(argv[cLoop][1]=='c') // check evolution index
				{
					if(temp>2) errcmd = 1;
					else if(cGotCmd<OPTION_EVCHK)
						cGotCmd = OPTION_EVCHK;
				}
				else if(argv[cLoop][1]=='s') // show chromosome fitness
				{
					if(temp>2) errcmd = 1;
					else if(cGotCmd<OPTION_SHOWF)
						cGotCmd = OPTION_SHOWF;
				}
				else if(argv[cLoop][1]=='y') // silent mode
				{
					if(temp>2) errcmd = 1;
					else debug = 0;
				}
				else if(argv[cLoop][1]=='h') // help?!
				{
					if(temp>2)
					{
						if(strcmp(&argv[cLoop][1],"help"))
							errcmd = 1;
					}
					else if(cGotCmd<OPTION_HELPU)
						cGotCmd = OPTION_HELPU;
				}
				else
				{
					errcmd = 1;
				}
				if(errcmd==1)
				{
					printf("Unknown option -%s\n",&argv[cLoop][1]);
				}
			}
			else
			{
				printf("Unknown parameter %s\n",argv[cLoop]);
				errcmd = 1;
			}
		}
	}

	if(errcmd==1&&cGotCmd!=OPTION_HELPU) cGotCmd = OPTION_NOCMD;

	switch(cGotCmd)
	{
		case OPTION_GVIEW:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_NOPOP;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			cPopulation.SaveFile();
			backval = cPopulation.PopulationSize();
			served = 1;
		}
		break;

		case OPTION_X2ARR:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_NOPOP;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			// check valid index to extract
			if(index<0||index>=cPopulation.PopulationSize())
			{
				printf("Invalid index specified!\n");
				return ERROR_EXT_EVO_INVALID;
			}
			if(debug)
				printf("Extracting chromosome at index [%d]\n",index);
			// write chromosome to vector file
			FILE* cFile = fopen(cFileSEC,"wt");
			if(cFile)
			{
				if(debug)
					printf("Writing to a file in C array format.\n");
				my1Genome* cGenome = cPopulation.GetGenome(index);
				int cCount = cGenome->GeneCount();
				int cType = cPopulation.GetGeneInfo()->mType;
				fprintf(cFile,"#ifndef __NETARRAYH__\n");
				fprintf(cFile,"#define __NETARRAYH__\n");
				fprintf(cFile,"#define NET_DATACOUNT %d\n",cCount);
				if(cType==GENE_FLOAT)
				{
					fprintf(cFile,"/* Type=GENE_FLOAT */\n");
					fprintf(cFile,"float cNetData[NET_DATACOUNT]=\n{\n");
					int cLine = 0;
					for(int cLoop=0;cLoop<cCount;cLoop++)
					{
						float cValue = cGenome->GetGene(cLoop).Data().mDecimal;
						fprintf(cFile,"%f",cValue);
						cLine++;
						if(cLine==C_ARRAY_DATA_PER_LINE)
						{
							cLine = 0;
							fprintf(cFile,",\n");
						}
						else if(cLoop==cCount-1)
							fprintf(cFile,"\n");
						else
							fprintf(cFile,", ");
					}
					fprintf(cFile,"};\n");
				}
				else
				{
					fprintf(cFile,"/* Type=GENE_INTEGER */\n");
					fprintf(cFile,"int cNetData[NET_DATACOUNT]=\n{\n");
					int cLine = 0;
					for(int cLoop=0;cLoop<cCount;cLoop++)
					{
						int cValue = cGenome->GetGene(cLoop).Data().mInteger;
						fprintf(cFile,"%d",cValue);
						cLine++;
						if(cLine==C_ARRAY_DATA_PER_LINE||cLoop==cCount-1)
						{
							cLine = 0;
							fprintf(cFile,"\n");
						}
						else
						{
							fprintf(cFile,", ");
						}
					}
					fprintf(cFile,"}\n");
				}
				fprintf(cFile,"#endif /* __NETARRAYH__ */\n");
				fclose(cFile);
			}
			else
			{
				printf("Cannot write file '%s'.\n",cFileSEC);
				return ERROR_EXT_W_VECFILE;
			}
			if(debug)
				printf("Written chromosome data (C array format) to file '%s'!\n",cFileSEC);
			served = 1;
		}
		break;

		case OPTION_X2VEC:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_NOPOP;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			// get index to extract
			if(index<0)
			{
				if(cPopulation.EvolutionReady())
				{
					if(debug)
						printf("Population should go through evolution!\n");
					return ERROR_EXT_EVO_MUST;
				}
				// check ongoing active population
				if(cPopulation.GetEvoActive()<0)
				{
					test = 1; // new extract - update file!
				}
				else
				{
					if(debug)
						printf("Ongoing population active. Re-extracting.\n");
				}
				// get index
				index = cPopulation.NextEvalIndex();
				if(index<0&&index>=cPopulation.PopulationSize())
				{
					printf("Invalid evolution index! Reset?!\n");
					return ERROR_EXT_EVO_INVALID;
				}
			}
			else
			{
				if(index>=cPopulation.PopulationSize())
				{
					printf("Invalid index specified [%d]!\n",index);
					return ERROR_EXT_EVO_INVALID;
				}
				if(debug)
					printf("Single index extraction -> [%d]\n",index);
			}
			// write chromosome to vector file
			FILE* cFile = fopen(cFileSEC,"wt");
			if(cFile)
			{
				if(debug)
					printf("Writing to vector file.\n");
				my1Genome* cGenome = cPopulation.GetGenome(index);
				int cCount = index;
				fprintf(cFile,"[VectorFile] ID=%d ",cCount); // ID - use index
				cCount = cGenome->GeneCount();
				fprintf(cFile," Size=%d ",cCount);  // # of items in vector
				int cType = cPopulation.GetGeneInfo()->mType;
				fprintf(cFile," Type=%d\n",cType);  // vector type
				for(int cLoop=0;cLoop<cCount;cLoop++)
				{
					if(cType==GENE_FLOAT)
					{
						float cValue = cGenome->GetGene(cLoop).Data().mDecimal;
						fprintf(cFile,"[Item%d]=%f",cLoop,cValue);
					}
					else
					{
						int cValue = cGenome->GetGene(cLoop).Data().mInteger;
						fprintf(cFile,"[Item%d]=%d",cLoop,cValue);
					}
					if((cLoop+1)%4) fprintf(cFile," ");
					else fprintf(cFile,"\n");
				}
				fclose(cFile);
			}
			else
			{
				printf("Cannot write vector file '%s'.\n",cFileSEC);
				return ERROR_EXT_W_VECFILE;
			}
			// update genetic file if necessary
			if(test)
			{
				// save population with updated evo info
				if(cPopulation.SaveFile(cFileName)<0)
				{
					printf("Cannot save population file '%s'.\n",cFileName);
					return ERROR_EXT_W_GENFILE;
				}
			}
			if(debug)
				printf("Extracted to vector file '%s'!\n",cFileSEC);
			served = 1;
		}
		break;

		case OPTION_D8FIT:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_NOPOP;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			// check if waiting active evolution
			index = cPopulation.GetEvoActive();
			if(index<0)
			{
				printf("No active evaluation.\n");
				return ERROR_UPD_EVAL_INACTIVE;
			}
			else if(index>=cPopulation.PopulationSize())
			{
				printf("Invalid evolution index.\n");
				return ERROR_UPD_EVO_INVALID;
			}
			// open fitness file
			FILE* cFile = fopen(cFileSEC,"rt");
			if(cFile)
			{
				if(debug)
					printf("Reading from fitness file.\n");
				int cIndex; float cValue;
				char cTest, cTestFileID[FILE_ID_STRLEN];
				fscanf(cFile,"%s",cTestFileID);
				do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
				fscanf(cFile,"%d",&cIndex); // fitness id
				do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
				fscanf(cFile,"%f",&cValue); // fitness value
				fclose(cFile);
				if(cIndex!=index)
				{
					printf("Invalid fitness index.\n");
					return ERROR_UPD_EVO_INVALID;
				}
				else if(strcmp(cTestFileID,"[FitnessFile]"))
				{
					printf("Invalid fitness file.\n");
					return ERROR_UPD_EVO_INVALID;
				}
				my1Genome* cGenome = cPopulation.GetGenome(cIndex);
				cGenome->SetFitness(cValue);
				if(cPopulation.DoneEvalIndex()<0)
				{
					if(debug)
						printf("Genome fitness updated!\n");
				}
				else
				{
					if(debug)
						printf("Something is wrong!\n");
				}
			}
			else
			{
				printf("Cannot open fitness file '%s'.\n",cFileSEC);
				return ERROR_UPD_R_FITFILE;
			}
			// save population with updated fitness
			if(cPopulation.SaveFile(cFileName)<0)
			{
				printf("Cannot save population file '%s'.\n",cFileName);
				return ERROR_UPD_W_GENFILE;
			}
			if(debug)
				printf("Updated from fitness file '%s'!\n",cFileSEC);
			served = 1;
		}
		break;

		case OPTION_GOEVO:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_NOPOP;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			// checking evolution readiness
			if(!cPopulation.EvolutionReady())
			{
				printf("Not ready for evolution.\n");
				return ERROR_EVO_NOT_READY;
			}
			// save prev population size (just in case)
			int cPopSize = cPopulation.PopulationSize();
			// try evolution!
			int cTemp = cPopulation.Evolution();
			if(cTemp<0)
			{
				printf("Evolution error!\n");
				return ERROR_EVO_EXEC_ERROR;
			}
			// write log if necessary
			if(cFileSEC[0]!=0x0)
			{
				int nofile = 0;
				FILE* cFile;

				if(debug)
					printf("Log file '%s' used.\n",cFileSEC);
				if(cTemp==1) // first generation info
				{
					cFile = fopen(cFileSEC,"wt"); // create new
					nofile = 1; // always print header
					if(debug) printf("First Gen - Need Header.\n");
				}
				else
				{
					cFile = fopen(cFileSEC,"rt");
					if(!cFile)
					{
						nofile = 1;
						cFile = fopen(cFileSEC,"wt"); // create new
						if(debug) printf("New file - Need Header.\n");
					}
					else
					{
						fclose(cFile);
						cFile = fopen(cFileSEC,"at"); // append existing
						if(debug) printf("Append log.\n");
					}
				}

				if(cFile)
				{
					if(nofile)
					{ // print header
						fprintf(cFile,"Index\tSize\tBest Fitness\t"
							"Avg. Fitness\tStd. Deviation\n");
					}
					fprintf(cFile,"%d\t",cTemp-1);
					fprintf(cFile,"%d\t",cPopSize);
					fprintf(cFile,"%f\t",cPopulation.GetPrevBestFitness());
					fprintf(cFile,"%f\t",cPopulation.GetPrevAvgFitness());
					fprintf(cFile,"%f\n",cPopulation.GetPrevStdDeviation());
					fclose(cFile);
				}
				else
				{
					if(debug)
						printf("Cannot write to file '%s'.\n",cFileSEC);
				}
			}

			if(debug)
				printf("New generation index = %d\n",cTemp);
			// save population with new generation
			if(cPopulation.SaveFile(cFileName)<0)
			{
				printf("Cannot save population file '%s'.\n",cFileName);
				return ERROR_EVO_W_GENFILE;
			}
			if(debug)
				printf("New generation in genetic file '%s'!\n",cFileName);
			backval = cTemp;
			served = 1;
		}
		break;

		case OPTION_EVRST:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_NOPOP;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			cPopulation.EvolutionReset();
			if(cPopulation.SaveFile(cFileName)<0)
			{
				printf("Cannot save genetic file '%s'.\n",cFileName);
				return ERROR_GEN_ERROR;
			}
			if(debug)
				printf("Evolution reset for genetic file '%s'!\n",cFileName);
			served = 1;
		}
		break;

		case OPTION_EVCHK:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_ERROR;
			}
			if(debug)
				printf("Genetic file '%s' used.\n",cFileName);
			// display evolution info
			int cCount = cPopulation.PopulationSize();
			if(debug)
			{
				printf("Population size is %d\n",cCount);
				printf("Active Eval. index is %d\n",cPopulation.GetEvoActive());
				printf("Evolution index is %d\n",cPopulation.GetEvalIndex());
				printf("Generation index is %d\n",cPopulation.GetGeneration());
				printf("Mutation Rate: %f\n",cPopulation.GetMutateRate());
				printf("Death Rate: %f\n",cPopulation.GetDeathRate());
				printf("Birth Rate: %f\n",cPopulation.GetBirthRate());
				int cTestIndex = -1;
				float cMaxVal = cPopulation.GetBestFitness(&cTestIndex);
				printf("Best fitness is %f at index %d\n",cMaxVal,cTestIndex);
			}
			backval = cPopulation.GetEvalIndex();
			served = 1;
		}
		break;

		case OPTION_SHOWF:
		{
			// load population file
			if(cPopulation.LoadFile(cFileName)<=0)
			{
				printf("Cannot load population file '%s'!\n",cFileName);
				return ERROR_GEN_ERROR;
			}
			if(index<0||index>=cPopulation.PopulationSize())
			{
				printf("Gen.Idx: %d => ",cPopulation.GetGeneration()-1);
				printf("BestFit: %f ",cPopulation.GetPrevBestFitness());
				printf("Avg.Fit: %f ",cPopulation.GetPrevAvgFitness());
				printf("Std.Dev: %f\n",cPopulation.GetPrevStdDeviation());
			}
			else
			{
				// display fitness info
				my1Genome* cGenome = cPopulation.GetGenome(index);
				float cValue = cGenome->GetFitness();
				printf("Genome Index: %d, Fitness: %f\n",index,cValue);
				backval = index;
			}
			served = 1;
		}
		break;

		case OPTION_HELPU:
		{
			printf("Genetic Algorithm Utility %s\n",MY1GENE_VERSION);
			printf("Usage: %s -<options> -f<filename> -i<index>\n",argv[0]);
			printf("Options:\n");
			printf("  p => Print population info\n");
			printf("  x => Extract population to vector file\n");
			printf("  a => Extract population to c array (for EYEBOT source code)\n");
			printf("  u => Update population from fitness file\n");
			printf("  e => Evolve current population to next generation\n");
			printf("  r => Reset evolution status\n");
			printf("  c => Check evolution status\n");
			printf("  s => Show chromosome fitness (with -i >=0)\n");
			printf("       Show previous evolution info (with -i <0)\n");
			printf("  y => Silent mode (Hide all info messages!)\n");
			printf("  h => Show this help\n");
			printf("Special:\n");
			printf("  f => Specify genetic file to extract/update\n");
			printf("  i => Specify index, no status update\n");
			printf("  >> x & a & u & e options can now specify filename as well\n");
			printf("If no options given, the utility starts in console mode.\n");
			served = 1;
		}
		break;

		case OPTION_NOCMD:
			if(errcmd)
			{
				printf("No known command found? Try with option -h.\n");
				served = 1;
			}
		break;

		default: // logically not possible
			printf("Unknown error!\n");
			served = 1;
		break;
	}

	if(served) return backval; // exit if done (request served)

	// console-mode variable
	my1Genome* pClone = NULL;
	char cTest;
	int cTemp, cSave=0;
	float cValue;

	if(argc>1) // filename specified??
	{
		if(cPopulation.LoadFile(cFileName)<=0)
		{
			printf("Cannot load genetic file '%s'\n",cFileName);
		}
	}

	while(1)
	{
		printf("\nGenetic Population Editor\n");
		printf("-------------------------\n");
		printf("Genetic file: %s\n\n",cFileName);
		cTemp = cPopulation.PopulationSize();
		if(cTemp)
		{
			printf("<S>ave Population\n");
			printf("<K>ill Population\n");
			printf("<P>rint Population\n");
			printf("<R>ank Population\n");
			printf("<E>dit Environment\n");
			printf("<V>iew Genome\n");
			if(!pClone)
				printf("<C>lone Genome\n");
			printf("<D>elete Genome\n");
		}
		else
		{
			printf("<C>reate Population\n");
			printf("<L>oad Population\n");
		}
		// if a chromosome is cloned!
		if(pClone)
		{
			printf("<A>lter Clone\n");
			printf("<M>utate Clone\n");
			printf("<I>nsert Clone\n");
			printf("<T>erminate Clone\n");
		}
		printf("<Q>uit Program\n\n");
		printf("Your Choice: ");
		cTest = LowerCase(getch());
		printf("%c\n",cTest);
		// filter response (avoid invalid selection)
		if(cTemp)
		{
			switch(cTest)
			{
				case 's': case 'k': case 'p': case 'r':
				case 'e': case 'v': case 'd': case 'q':
					break;
				case 'a': case 'm': case 'i': case 't':
					if(!pClone) cTest = ' ';
					break;
				case 'c':
					if(pClone) cTest = ' ';
					break;
				default:
					cTest = ' ';
			}
		}
		else
		{
			switch(cTest)
			{
				case 'c': case 'l': case 'q':
					break;
				case 'a': case 'm': case 'i': case 't':
					if(!pClone) cTest = ' ';
					break;
				default:
					cTest = ' ';
			}
		}
		if(cTest=='q')
		{
			if(cSave)
			{
				printf("Save Population? <y/n>: ");
				cTest = LowerCase(getch());
				printf("%c\n",cTest);
				if(cTest=='y')
				{
					cTemp = cPopulation.SaveFile(cFileName);
					printf("SavePopulation: %d\n",cTemp);
				}
			}
			break; // exit main loop
		}
		else if(cTest=='c')
		{
			if(cTemp)
			{
				printf("Number of Genome: %d\n",cTemp);
				printf("Select Genome Index: ");
				scanf("%d",&cTemp);
				if(cTemp<0) continue;
				my1Genome* cGenome = cPopulation.GetGenome(cTemp);
				cValue = cGenome->GetFitness();
				printf("Genome %d: Fitness=%f\n",cTemp,cValue);
				// if(pClone) delete pClone;
				pClone = new my1Genome(*cGenome);
				pClone->SetFitness(0.0);
				printf("Genome cloned!\n");
				continue;
			}

			my1GenePoolInfo cInfo;
			// get population info
			do {
				printf("Enter Gene Type (0-Integer, 1-Float): ");
				scanf("%d",&cTemp);
			} while(cTemp!=1&&cTemp!=0);
			cInfo.mGeneInfo.mType = (my1GeneType) cTemp;
			if(cTemp==0)
			{
				printf("Enter Min Value (Integer): ");
				scanf("%d",&cTemp);
				cInfo.mGeneInfo.mMin.mInteger = cTemp;
				printf("Enter Max Value (Integer): ");
				scanf("%d",&cTemp);
				cInfo.mGeneInfo.mMax.mInteger = cTemp;
			}
			else // float
			{
				printf("Enter Min Value (Float): ");
				scanf("%f",&cValue);
				cInfo.mGeneInfo.mMin.mDecimal = cValue;
				printf("Enter Max Value (Float): ");
				scanf("%f",&cValue);
				cInfo.mGeneInfo.mMax.mDecimal = cValue;
			}
			cInfo.mGeneInfo.Calculate();
			printf("Enter Gene Count: ");
			scanf("%d",&cTemp);
			cInfo.mGeneCount = cTemp;
			printf("Enter Genome Count: ");
			scanf("%d",&cTemp);
			cInfo.mGenomeCount = cTemp;
			// create population
			cTemp = CreatePopulation(&cPopulation,&cInfo);
			printf("CreatePopulation: %d\n",cTemp);
			if(cTemp>0) cSave = 1;
		}
		else if(cTest=='l')
		{
			cTemp = cPopulation.LoadFile(cFileName);
			printf("LoadPopulation: %d\n",cTemp);
		}
		else if(cTest=='s')
		{
			cTemp = cPopulation.SaveFile(cFileName);
			printf("SavePopulation: %d\n",cTemp);
			if(cTemp>=0) cSave = 0;
		}
		else if(cTest=='k')
		{
			int cKill;
			printf("Total Genome: %d\n",cTemp);
			printf("Number of Genome to kill: ");
			scanf("%d",&cKill);
			if(cKill>=cTemp)
			{
				printf("Kill ALL Population. Sure? <y/n>: ");
				cTest = LowerCase(getch());
				printf("%c\n",cTest);
				if(cTest=='y')
				{
					cPopulation.KillPopulation();
					cTemp = cPopulation.PopulationSize();
					printf("Population: %d\n",cTemp);
					if(pClone)
					{
						delete pClone;
						pClone = NULL;
						printf("Clone terminated!\n");
					}
					if(cTemp==0) cSave = 0;
				}
			}
			else if(cKill>0)
			{
				int cStart = cKill;
				while(cKill)
				{
					cPopulation.DeleteGenome(--cTemp);
					cKill--;
				}
				cTemp = cPopulation.PopulationSize();
				printf("Kill Left: %d\t",cKill);
				printf("Population: %d\n",cTemp);
				if(cKill<cStart) cSave = 1;
			}
		}
		else if(cTest=='p')
		{
			cPopulation.SaveFile();
			WaitKey();
		}
		else if(cTest=='r')
		{
			if(cPopulation.EvolutionReady())
			{
				printf("Sort first? <y/n>: ");
				cTest = LowerCase(getch());
				printf("%c\n",cTest);
				if(cTest=='y')
				{
					cPopulation.SortPopulation();
					cSave = 1;
				}
			}
			printf("List for population & fitness:\n");
			for(int cLoop=0;cLoop<cTemp;cLoop++)
			{
				my1Genome* cGenome = cPopulation.GetGenome(cLoop);
				float cValue = cGenome->GetFitness();
				printf("Genome %3d\t: Fitness = %f\n",cLoop,cValue);
				if((cLoop+1)%24==0||cLoop==cTemp-1) WaitKey();
			}
		}
		else if(cTest=='e')
		{
			while(1)
			{
				printf("\nCurrent Environment Settings\n");
				printf("****************************\n\n");
				printf("<M>utation Rate: %f\n",cPopulation.GetMutateRate());
				printf("<D>eath Rate: %f\n",cPopulation.GetDeathRate());
				printf("<B>irth Rate: %f\n",cPopulation.GetBirthRate());

				printf("Change which setting? ('q' to quit): ");
				cTest = LowerCase(getch());
				printf("%c\n",cTest);

				if(cTest=='q')
				{
					break;
				}
				else if(cTest=='m')
				{
					printf("Enter NEW Mutation Rate: ");
					scanf("%f",&cValue);
					printf("New Rate: %f. Change? <y/n>: ",cValue);
					cTest = LowerCase(getch());
					printf("%c\n",cTest);
					if(cTest=='y')
					{
						cPopulation.SetMutateRate(cValue);
						cSave = 1;
					}
				}
				else if(cTest=='d')
				{
					printf("Enter NEW Death Rate: ");
					scanf("%f",&cValue);
					printf("New Rate: %f. Change? <y/n>: ",cValue);
					cTest = LowerCase(getch());
					printf("%c\n",cTest);
					if(cTest=='y')
					{
						cPopulation.SetDeathRate(cValue);
						cSave = 1;
					}
				}
				else if(cTest=='b')
				{
					printf("Enter NEW Birth Rate: ");
					scanf("%f",&cValue);
					printf("New Rate: %f. Change? <y/n>: ",cValue);
					cTest = LowerCase(getch());
					printf("%c\n",cTest);
					if(cTest=='y')
					{
						cPopulation.SetBirthRate(cValue);
						cSave = 1;
					}
				}
				else
					printf("Unknown option. <q> to quit.\n");
			}

		}
		else if(cTest=='v')
		{
			printf("Number of Genome: %d\n",cTemp);
			printf("Select Genome Index: ");
			scanf("%d",&cTemp);
			if(cTemp<0) continue;
			my1Genome* cGenome = cPopulation.GetGenome(cTemp);
			cValue = cGenome->GetFitness();
			printf("Genome %d: Fitness=%f\n",cTemp,cValue);
			int cCount = cGenome->GeneCount();
			int cLoop=0;
			printf("Show all gene(s)? <y/n>: ");
			cTest = LowerCase(getch());
			printf("%c\n",cTest);
			if(cTest=='y')
			{
				while(cLoop<cCount)
				{
					my1Gene* cGene = (my1Gene*) &cGenome->GetGene(cLoop);
					if(cGene->Type()==GENE_FLOAT)
						printf("{%d=%f}",cLoop,cGene->Data().mDecimal);
					else
						printf("{%d=%d}",cLoop,cGene->Data().mInteger);
					cLoop++;
					if(cLoop==cCount) printf("\n");
					else printf("\t");
				}
			}
			while(1)
			{
				printf("Number of Genes: %d\n",cCount);
				printf("Select Gene Index (-1 to exit): ");
				scanf("%d",&cLoop);
				if(cLoop<0) break;
				printf("Gene Index: %d - ",cLoop);
				my1Gene* cGene = (my1Gene*) &cGenome->GetGene(cLoop);
				if(cGene->Type()==GENE_FLOAT)
					printf("Value: %f\n",cGene->Data().mDecimal);
				else
					printf("Value: %d\n",cGene->Data().mInteger);
			}
		}
		else if(cTest=='d')
		{
			printf("Number of Genome: %d\n",cTemp);
			printf("Select Genome Index: ");
			scanf("%d",&cTemp);
			if(cTemp<0) continue;
			my1Genome* cGenome = cPopulation.GetGenome(cTemp);
			cValue = cGenome->GetFitness();
			printf("Genome %d: Fitness=%f\n",cTemp,cValue);

			printf("Delete this chromosome? <y/n>: ");
			cTest = LowerCase(getch());
			printf("%c\n",cTest);
			if(cTest=='y')
			{
				if(cPopulation.DeleteGenome(cTemp)>=0)
				{
					printf("Genome deleted!\n");
					cSave = 1;
				}
				else
				{
					printf("Error deleting chromosome!\n");
				}
			}
		}
		else if(cTest=='a')
		{
			my1Genome* cGenome = pClone;
			int cCount = cGenome->GeneCount();
			int cLoop=0;
			printf("Show all gene(s)? <y/n>: ");
			cTest = LowerCase(getch());
			printf("%c\n",cTest);
			if(cTest=='y')
			{
				while(cLoop<cCount)
				{
					my1Gene* cGene = (my1Gene*) &cGenome->GetGene(cLoop);
					if(cGene->Type()==GENE_FLOAT)
						printf("{%d=%f}",cLoop,cGene->Data().mDecimal);
					else
						printf("{%d=%d}",cLoop,cGene->Data().mInteger);
					cLoop++;
					if(cLoop==cCount) printf("\n");
					else printf("\t");
				}
			}
			while(1)
			{
				printf("Number of Genes: %d\n",cCount);
				printf("Select Gene Index (-1 to exit): ");
				scanf("%d",&cLoop);
				if(cLoop<0) break;
				my1GeneData cValue;
				my1Gene* cGene = (my1Gene*) &cGenome->GetGene(cLoop);
				if(cGene->Type()==GENE_FLOAT)
				{
					printf("Value: %f\n",cGene->Data().mDecimal);
					printf("Enter NEW Value: ");
					scanf("%f",&cValue.mDecimal);
					printf("New Value: %f. Change? <y/n>: ",cValue.mDecimal);
					cTest = LowerCase(getch());
					printf("%c\n",cTest);
					if(cTest=='y')
						cGene->Data(cValue);
				}
				else
				{
					printf("Value: %d\n",cGene->Data().mInteger);
					printf("Enter NEW Value: ");
					scanf("%d",&cValue.mInteger);
					printf("New Value: %d. Change? <y/n>: ",cValue.mInteger);
					cTest = LowerCase(getch());
					printf("%c\n",cTest);
					if(cTest=='y')
						cGene->Data(cValue);
				}
			}
		}
		else if(cTest=='m')
		{
			printf("Confirm Mutation? <y/n>: ");
			cTest = LowerCase(getch());
			printf("%c\n",cTest);
			if(cTest=='y')
			{
				pClone->Mutation();
				printf("Clone mutation completed\n");
			}
		}
		else if(cTest=='i')
		{
			printf("Number of Genome: %d\n",cTemp);
			printf("Select Clone Insertion Index: ");
			scanf("%d",&cTemp);

			printf("Confirm insertion? <y/n>: ");
			cTest = LowerCase(getch());
			printf("%c\n",cTest);
			if(cTest=='y')
			{
				my1Genome* cGenome = new my1Genome(*pClone);
				if(cPopulation.InsertGenome(cTemp,cGenome)>=0)
				{
					printf("Clone inserted!\n");
					cSave = 1;
				}
				else
				{
					printf("Error inserting clone!\n");
				}
				delete cGenome;
			}
		}
		else if(cTest=='t')
		{
			printf("Confirm Clone Termination? <y/n>: ");
			cTest = LowerCase(getch());
			printf("%c\n",cTest);
			if(cTest=='y')
			{
				delete pClone;
				pClone = NULL;
				printf("Clone terminated!\n");
			}
		}
		else
		{
			printf("Unknown option!\n");
		}
	}
	if(pClone) delete pClone;

	return 0;
}
//------------------------------------------------------------------------------
