//------------------------------------------------------------------------------
#include "my1gene.hpp"
//------------------------------------------------------------------------------
#include <cstdlib>
using std::rand; // used by my1Gene::Random
//------------------------------------------------------------------------------
#include <cmath>
using std::sqrt; // used by my1GenePool::Evolution
//------------------------------------------------------------------------------
#include <cstring>
using std::strncmp; // used by my1GenePool::{Load,Save}File
//------------------------------------------------------------------------------
#include <cstdio>
using std::FILE;
using std::fprintf;
using std::fscanf;
//------------------------------------------------------------------------------
my1Gene::my1Gene(my1GeneInfo *pInfo)
{
	mData.mInteger = 0; // clear data - float 'auto'-cleared?
	if(pInfo)
	{
		mInfo = pInfo;
		mLocalInfo = false;
	}
	else
	{
		mInfo = new my1GeneInfo;
		mLocalInfo = true;
	}
}
//------------------------------------------------------------------------------
my1Gene::my1Gene(const my1Gene& aGene)
{
	my1Gene& cGene = const_cast<my1Gene&>(aGene);
	mData = cGene.mData;
	if(cGene.mLocalInfo)
	{
		mInfo = new my1GeneInfo;
		*mInfo = *cGene.mInfo;
		mLocalInfo = true;
	} else {
		mInfo = cGene.mInfo;
		mLocalInfo = false;
	}
}
//------------------------------------------------------------------------------
my1Gene::~my1Gene()
{
	if(mLocalInfo) delete mInfo;
}
//------------------------------------------------------------------------------
my1GeneType my1Gene::Type(void)
{
	return mInfo->mType;
}
//------------------------------------------------------------------------------
my1GeneData& my1Gene::Data(void)
{
	return mData;
}
//------------------------------------------------------------------------------
void my1Gene::Data(my1GeneData& aData)
{
	mData = aData;
}
//------------------------------------------------------------------------------
void my1Gene::Mutate(void)
{
	my1GeneData cData;
	if(mInfo->mType==GENE_FLOAT)
	{
		cData.mDecimal = mData.mDecimal; // save current value
		this->Random(); // Random() changes current value
		if(cData.mDecimal<mInfo->mMid.mDecimal)
		{
			if(mData.mDecimal<mInfo->mMid.mDecimal)
			{
				// invert random value if same 'polarity'
				mData.mDecimal = mInfo->mMax.mDecimal -
					(mData.mDecimal - mInfo->mMin.mDecimal);
			}
		}
		else if(cData.mDecimal>mInfo->mMid.mDecimal)
		{
			if(mData.mDecimal>mInfo->mMid.mDecimal)
			{
				// invert random value if same 'polarity'
				mData.mDecimal = mInfo->mMin.mDecimal +
					(mInfo->mMax.mDecimal - mData.mDecimal);
			}
		}
		else
		{
			if(mData.mDecimal==cData.mDecimal)
				mData.mDecimal=mInfo->mMax.mDecimal;
		}
	}
	else // if(mInfo->mType==GENE_INTEGER)
	{
		cData.mInteger = mData.mInteger; // save current value
		this->Random(); // Random() changes current value
		if(cData.mInteger<mInfo->mMid.mInteger)
		{
			if(mData.mInteger<mInfo->mMid.mInteger)
			{
				// invert random value if same 'polarity'
				mData.mInteger = mInfo->mMax.mInteger -
					(mData.mInteger - mInfo->mMin.mInteger);
			}
		}
		else if(cData.mInteger>mInfo->mMid.mInteger)
		{
			if(mData.mInteger>mInfo->mMid.mInteger)
			{
				// invert random value if same 'polarity'
				mData.mInteger = mInfo->mMin.mInteger +
					(mInfo->mMax.mInteger - mData.mInteger);
			}
		}
		else
		{
			if(mData.mInteger==cData.mInteger)
				mData.mInteger=mInfo->mMax.mInteger;
		}
	}
}
//------------------------------------------------------------------------------
float my1Gene::Random(void)
{
	float cValueNew = (float)rand()/RAND_MAX;
	if(mInfo->mType==GENE_FLOAT)
	{
		float cOffset = cValueNew * mInfo->mRange.mDecimal;
		mData.mDecimal = cOffset + mInfo->mMin.mDecimal;
	}
	// if(mInfo->mType==GENE_INTEGER)
	{
		float cTest = cValueNew * (float)mInfo->mRange.mInteger;
		int cOffset = (int) cTest; // always round down by default
		cTest -= (float) cOffset; // get decimal points
		if(cTest>=0.5) cOffset++; // check for round up
		mData.mInteger = cOffset + mInfo->mMin.mInteger;
	}
	return cValueNew; // for other method(s) to use
}
//------------------------------------------------------------------------------
my1Genome::my1Genome()
{
	mFitness = IMPOSSIBLY_UNFIT;
}
//------------------------------------------------------------------------------
my1Genome::my1Genome(const my1Genome& aGenome)
{
	my1Genome& cGenome = const_cast<my1Genome&>(aGenome);
	int cCount = cGenome.mGenes.size();
	for(int cLoop=0; cLoop<cCount; cLoop++)
		mGenes.push_back(cGenome.mGenes[cLoop]);
	mFitness = cGenome.mFitness;
}
//------------------------------------------------------------------------------
my1Genome::~my1Genome()
{
	// nothing to do! mGenes destructor would clean itself up!
	//mGenes.clear();
}
//------------------------------------------------------------------------------
int my1Genome::GeneCount(void)
{
	return mGenes.size();
}
//------------------------------------------------------------------------------
my1Gene& my1Genome::GetGene(int anIndex)
{
	return mGenes[anIndex]; // assume valid index!
}
//------------------------------------------------------------------------------
int my1Genome::InsertGene(int anIndex, const my1Gene& aGene)
{
	//mGenes.push_back(aGene);
	mGenes.insert(mGenes.begin()+anIndex,aGene);
	return mGenes.size();
}
//------------------------------------------------------------------------------
int my1Genome::DeleteGene(int anIndex)
{
	//mGenes.pop_back(aGene);
	mGenes.erase(mGenes.begin()+anIndex);
	return mGenes.size();
}
//------------------------------------------------------------------------------
float my1Genome::GetFitness(void)
{
	return mFitness;
}
//------------------------------------------------------------------------------
void my1Genome::SetFitness(float aValue)
{
	mFitness = aValue;
}
//------------------------------------------------------------------------------
void my1Genome::Crossover(const my1Genome& aGenome)
{
	my1Genome& cGenome = const_cast<my1Genome&>(aGenome);
	int cCount = mGenes.size();
	int cPoint = rand() % cCount;
	if(cPoint == 0) cPoint = cCount/2;
	// assume aGenome has the same type & size
	for(int cLoop=cPoint; cLoop<cCount; cLoop++)
	{
		my1Gene cSwap = mGenes[cLoop];
		mGenes[cLoop] = cGenome.mGenes[cLoop];
		cGenome.mGenes[cLoop] = cSwap;
	}
}
//------------------------------------------------------------------------------
void my1Genome::Mutation(void)
{
	int cSize = mGenes.size();
	int cCount = (int)(((float)rand()/RAND_MAX)*cSize);
	vector<bool> cMutatedGene;
	// mark mutated genes later!
	for(int cLoop=0; cLoop<cSize; cLoop++)
		cMutatedGene.push_back(false);
	cCount /= 2; // max mutate half the genes
	if(cCount==0) cCount = 1; // at least 1 gene MUST mutate
	for(int cLoop=0; cLoop<cCount; cLoop++)
	{
		int cIndex;
		do { cIndex = rand()%cSize;
		} while(cMutatedGene[cIndex]);
		this->GetGene(cIndex).Mutate();
		cMutatedGene[cIndex] = true;
	}
}
//------------------------------------------------------------------------------
bool my1Genome::operator<(const my1Genome& aGenome) // sort needs <
{
	my1Genome& cGenome = const_cast<my1Genome&>(aGenome);
	return mFitness > cGenome.mFitness; // to get descending order
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
my1GenePool::my1GenePool()
{
	mMutateRate = DEFAULT_MUTATERATE;
	mDeathRate = DEFAULT_DEATHRATE;
	mBirthRate = DEFAULT_BIRTHRATE;
	this->ResetStatistic();
}
//------------------------------------------------------------------------------
my1GenePool::~my1GenePool()
{
	mGenomes.clear();
}
//------------------------------------------------------------------------------
int my1GenePool::PopulationSize(void)
{
	return mGenomes.size();
}
//------------------------------------------------------------------------------
void my1GenePool::KillPopulation(void)
{
	mGenomes.clear();
	this->ResetStatistic();
}
//------------------------------------------------------------------------------
void my1GenePool::SortPopulation(void)
{
	mGenomes.sort();
}
//------------------------------------------------------------------------------
my1Genome* my1GenePool::GetGenome(int anIndex)
{
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	int cIndex = 0;
	while(pGenome!=mGenomes.end()&&cIndex<anIndex)
	{
		pGenome++; cIndex++;
	}
	return &(*pGenome); // assume valid index
}
//------------------------------------------------------------------------------
int my1GenePool::InsertGenome(int anIndex,my1Genome* aGenome)
{
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	int cIndex = 0;
	while(pGenome!=mGenomes.end()&&cIndex<anIndex)
	{
		pGenome++; cIndex++;
	}
	if(cIndex==anIndex)
		mGenomes.insert(pGenome,*aGenome);
	else
		cIndex = -1;
	return cIndex;
}
//------------------------------------------------------------------------------
int my1GenePool::DeleteGenome(int anIndex)
{
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	int cIndex = 0;
	while(pGenome!=mGenomes.end()&&cIndex<anIndex)
	{
		pGenome++; cIndex++;
	}
	if(cIndex==anIndex)
	{
		mGenomes.erase(pGenome);
	}
	else
		cIndex = -1;
	return cIndex;
}
//------------------------------------------------------------------------------
my1GeneInfo* my1GenePool::GetGeneInfo(void)
{
	return &mInfo;
}
//------------------------------------------------------------------------------
int my1GenePool::GetEvoActive(void)
{
	return mEvoActive;
}
//------------------------------------------------------------------------------
int my1GenePool::GetEvalIndex(void)
{
	return mEvalIndex;
}
//------------------------------------------------------------------------------
int my1GenePool::GetGeneration(void)
{
	return mGeneration;
}
//------------------------------------------------------------------------------
void my1GenePool::ResetStatistic(void)
{
	mEvoActive = -1; mEvalIndex = 0; mGeneration = 0;
	mPrevBestFit = 0.0; mPrevAvgFit = 0.0; mPrevStdDev = 0.0;
	mPrevMutated = 0;
}
//------------------------------------------------------------------------------
float my1GenePool::GetMutateRate(void)
{
	return mMutateRate;
}
//------------------------------------------------------------------------------
void my1GenePool::SetMutateRate(float aRate)
{
	mMutateRate = aRate;
}
//------------------------------------------------------------------------------
float my1GenePool::GetDeathRate(void)
{
	return mDeathRate;
}
//------------------------------------------------------------------------------
void my1GenePool::SetDeathRate(float aRate)
{
	mDeathRate = aRate;
}
//------------------------------------------------------------------------------
float my1GenePool::GetBirthRate(void)
{
	return mBirthRate;
}
//------------------------------------------------------------------------------
void my1GenePool::SetBirthRate(float aRate)
{
	mBirthRate = aRate;
}
//------------------------------------------------------------------------------
float my1GenePool::GetPrevBestFitness(void)
{
	return mPrevBestFit;
}
//------------------------------------------------------------------------------
float my1GenePool::GetPrevAvgFitness(void)
{
	return mPrevAvgFit;
}
//------------------------------------------------------------------------------
float my1GenePool::GetPrevStdDeviation(void)
{
	return mPrevStdDev;
}
//------------------------------------------------------------------------------
int my1GenePool::GetPrevMutated(void)
{
	return mPrevMutated;
}
//------------------------------------------------------------------------------
float my1GenePool::GetBestFitness(int* anIndex)
{
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	int cCount = 0, cIndex = 0;
	float cValue, cMaxValue = IMPOSSIBLY_UNFIT;
	int cGroup = mGenomes.size();
	if(mEvalIndex) cGroup = mEvalIndex;
	while(pGenome!=mGenomes.end()&&cCount<cGroup)
	{
		my1Genome* cGenome = &(*pGenome);
		cValue = cGenome->GetFitness();
		if(cCount)
		{
			if(cValue>cMaxValue)
			{
				cIndex = cCount;
				cMaxValue = cValue;
			}
		}
		else
		{
			cMaxValue = cValue;
		}
		pGenome++; cCount++;
	}
	if(anIndex) *anIndex = cIndex;
	return cMaxValue;
}
//------------------------------------------------------------------------------
float my1GenePool::GetAvgFitness(void)
{
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	int cCount = 0; float cValue = 0.0;
	int cGroup = mGenomes.size();
	if(mEvalIndex) cGroup = mEvalIndex;
	while(pGenome!=mGenomes.end()&&cCount<cGroup)
	{
		my1Genome* cGenome = &(*pGenome);
		cValue += cGenome->GetFitness();
		pGenome++; cCount++;
	}
	if(cCount) cValue /= (float) cCount;
	return cValue;
}
//------------------------------------------------------------------------------
int my1GenePool::NextEvalIndex(void)
{
	if(mEvoActive<0&&mEvalIndex<(int)mGenomes.size())
	{
		mEvoActive = mEvalIndex;
	}
	return mEvoActive;
}
//------------------------------------------------------------------------------
int my1GenePool::DoneEvalIndex(void)
{
	if(mEvoActive==mEvalIndex&&mEvalIndex<(int)mGenomes.size())
	{
		mEvalIndex++;
		mEvoActive = -1;
	}
	return mEvoActive;
}
//------------------------------------------------------------------------------
void my1GenePool::EvolutionReset(void)
{
	mEvoActive = -1;
	mEvalIndex = 0;
}
//------------------------------------------------------------------------------
bool my1GenePool::EvolutionReady(void)
{
	return mEvalIndex==(int)mGenomes.size();
}
//------------------------------------------------------------------------------
int my1GenePool::Evolution(void)
{
	if(mEvoActive>=0||mEvalIndex<(int)mGenomes.size()) return -1;
	if(!mGenomes.size()) return -2;
	SortPopulation(); // do this first! easier to get best fitness
	// store best/average fitness info
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	int cLoop = 0, cSize = mGenomes.size();
	// find average
	mPrevBestFit = pGenome->GetFitness();
	mPrevAvgFit = mPrevBestFit;
	pGenome++; cLoop++;
	while(pGenome!=mGenomes.end())
	{
		mPrevAvgFit += pGenome->GetFitness();
		pGenome++; cLoop++;
	}
	if(cLoop) mPrevAvgFit /= (float) cLoop;
	// now calculate std deviation
	pGenome = mGenomes.begin(); cLoop = 0;
	mPrevStdDev = 0.0;
	while(pGenome!=mGenomes.end())
	{
		float cTemp = pGenome->GetFitness();
		cTemp -= mPrevAvgFit; // deviation from mean
		cTemp *= cTemp; // square value
		mPrevStdDev += cTemp; // accumulate
		pGenome++; cLoop++;
	}
	if(cLoop) mPrevStdDev /= (float) cLoop;
	mPrevStdDev = sqrt(mPrevStdDev);
	//  kill weak chromosomes
	int cKill = (int) (cSize * mDeathRate);
	int cTempSize = cSize;
	for(cLoop=0; cLoop<cKill; cLoop++)
	{
		DeleteGenome(--cTempSize);
	}
	// cross-over most-fit with next-in-line & add to population
	int cLive = (int) (cSize * mBirthRate);
	int cIndex = 1; cLoop = 0;
	while(cLoop<cLive)
	{
		if(cIndex==cTempSize) cIndex--;
		my1Genome cGenome(*GetGenome(0));
		my1Genome dGenome(*GetGenome(cIndex));
		cGenome.SetFitness(0.0);
		dGenome.SetFitness(0.0);
		cGenome.Crossover(dGenome);
		mGenomes.insert(mGenomes.end(),cGenome);
		cIndex++; cLoop++;
		if(cLoop==cLive)
			break;
		mGenomes.insert(mGenomes.end(),dGenome);
		cLoop++;
	}
	//  mutate new-born population only!
	cSize = mGenomes.size(); // get new population size
	int cMutate = (int) (cLive * mMutateRate);
	cIndex = cSize - 1; cLoop = 0;
	while(cIndex>0&&cLoop<cMutate) // fittest population will never mutate
	{
		my1Genome* aGenome = GetGenome(cIndex);
		aGenome->SetFitness(0.0);
		aGenome->Mutation();
		cIndex--; cLoop++;
	}
	mPrevMutated = cLoop;
	mEvalIndex = 0; // reset - all chromo has been evaluated
	mGeneration++; // increment generation count
	return mGeneration;
}
//------------------------------------------------------------------------------
int my1GenePool::LoadFile(char* aFilename)
{
	FILE* cFile = fopen(aFilename,"rt");
	if(!cFile)
		return -1; // cannot open file
	char cTestID[MY1GENE_IDSIZE];
	fgets(cTestID,MY1GENE_IDSIZE,cFile);
	if(strncmp(cTestID,MY1GENE_FILEID,MY1GENE_IDSIZE))
	{
		fclose(cFile);
		return -2; // unknown file format
	}
	char cTest;
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%s",cTestID);
	if(strncmp(cTestID,MY1GENE_VERSION,MY1GENE_IDSIZE))
	{
		fclose(cFile);
		return -3; // different genetic version
	}
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%d",&mEvoActive);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%d",&mEvalIndex);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%d",&mGeneration);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%f",&mMutateRate);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%f",&mDeathRate);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%f",&mBirthRate);
	int cSize;
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%d",&cSize);
	int cType;
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%d",&cType);
	if(cType==GENE_FLOAT)
	{
		mInfo.mType = GENE_FLOAT;
		do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
		fscanf(cFile,"%f",&mInfo.mMin.mDecimal);
		do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
		fscanf(cFile,"%f",&mInfo.mMax.mDecimal);
	}
	else
	{
		mInfo.mType = GENE_INTEGER;
		do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
		fscanf(cFile,"%d",&mInfo.mMin.mInteger);
		do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
		fscanf(cFile,"%d",&mInfo.mMax.mInteger);
	}
	mInfo.Calculate();
	for(int cLoop=0;cLoop<cSize;cLoop++)
	{
		my1Genome cGenome;
		do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
		my1GeneData cValue;
		fscanf(cFile,"%f",&cValue.mDecimal);
		cGenome.SetFitness(cValue.mDecimal);
		do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
		int cSizeC;
		fscanf(cFile,"%d",&cSizeC);
		for(int cLoopC=0;cLoopC<cSizeC;cLoopC++)
		{
			my1Gene cGene(&mInfo);
			do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
			if(cType==GENE_FLOAT)
				fscanf(cFile,"%f",&cValue.mDecimal);
			else
				fscanf(cFile,"%d",&cValue.mInteger);
			cGene.Data(cValue);
			cGenome.InsertGene(cLoopC,cGene);
		}
		this->InsertGenome(mGenomes.size(),&cGenome);
	}
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%f",&mPrevBestFit);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%f",&mPrevAvgFit);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%f",&mPrevStdDev);
	do { fscanf(cFile,"%c",&cTest); } while(cTest!='=');
	fscanf(cFile,"%d",&mPrevMutated);
	fclose(cFile);
	return mGenomes.size();
}
//------------------------------------------------------------------------------
int my1GenePool::SaveFile(char* aFilename)
{
	if(!mGenomes.size()) return -2;
	FILE* cFile; bool cPrint = false;
	if(aFilename)
	{
		cFile = fopen(aFilename,"wt");
		if(!cFile) return -1; // cannot open file
	}
	else
	{
		cFile = stdout;
		cPrint = true;
	}
	if(cPrint)
	{
		fprintf(cFile,"\n*****************************\n");
		fprintf(cFile,"Start Genetic Population Info\n");
		fprintf(cFile,"*****************************\n");
	}
	else
	{
		fprintf(cFile,"%s VERSION=%s ",MY1GENE_FILEID,MY1GENE_VERSION);
	}
	fprintf(cFile,"Active Eval. Index=%d ",mEvoActive);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Evolution Index=%d ",mEvalIndex);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Generation Count=%d\n",mGeneration);
	fprintf(cFile,"Mutation Rate=%f ",mMutateRate);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Death Rate=%f ",mDeathRate);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Birth Rate=%f\n",mBirthRate);
	int cSize = mGenomes.size();
	if(!cPrint) fprintf(cFile,"[Data] ");
	fprintf(cFile,"Population Size=%d ",cSize);
	int cType = mInfo.mType;
	fprintf(cFile,"Gene Type=%d ",cType);
	if(cPrint) fprintf(cFile,"\n ");
	if(cType==GENE_FLOAT)
	{
		fprintf(cFile,"( Min=%f , ",mInfo.mMin.mDecimal);
		fprintf(cFile,"Max=%f )\n",mInfo.mMax.mDecimal);
	}
	else
	{
		fprintf(cFile,"( Min=%d , ",mInfo.mMin.mInteger);
		fprintf(cFile,"Max=%d )\n",mInfo.mMax.mInteger);
	}
	if(cPrint)
	{
		cSize = -1;
		float cBestFit = this->GetBestFitness(&cSize);
		fprintf(cFile,"Curr. Best Fitness=%f\n (Index=%d)\n",cBestFit,cSize);
		cSize = 0; // skip printing ALL chromosome info
	}
	list<my1Genome>::iterator pGenome = mGenomes.begin();
	for(int cLoop=0;cLoop<cSize;cLoop++)
	{
		if(pGenome==mGenomes.end())
		{
			fprintf(cFile,"Unexpected error!\n");
			break;
		}
		my1Genome* cGenome = &(*pGenome);
		fprintf(cFile,"{%d}\t",cLoop);
		fprintf(cFile,"[C] ");
		fprintf(cFile,"Fitness=%f ",cGenome->GetFitness());
		int cSizeC = cGenome->GeneCount();
		fprintf(cFile,"GeneCount=%d\n",cSizeC);
		for(int cLoopC=0;cLoopC<cSizeC;cLoopC++)
		{
			my1Gene* cGene = &(cGenome->GetGene(cLoopC));
			fprintf(cFile,"{%d}\t",cLoopC);
			fprintf(cFile,"[G] ");
			if(cType==GENE_FLOAT)
				fprintf(cFile,"<=%f\n",cGene->Data().mDecimal);
			else
				fprintf(cFile,"<=%d\n",cGene->Data().mInteger);
		}
		pGenome++;
	}
	fprintf(cFile,"Prev. Best Fitness=%f ",mPrevBestFit);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Prev. Avg. Fitness=%f ",mPrevAvgFit);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Prev. Std. Deviation=%f",mPrevStdDev);
	if(cPrint) fprintf(cFile,"\n");
	fprintf(cFile,"Prev. Mutated Birth=%d\n",mPrevMutated);
	if(cPrint)
	{
		fprintf(cFile,"*****************************\n");
		fprintf(cFile,"End  Genetic Population  Info\n");
		fprintf(cFile,"*****************************\n");
	}
	else
	{
		fclose(cFile);
	}
	return mGenomes.size();
}
//------------------------------------------------------------------------------
