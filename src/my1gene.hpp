//------------------------------------------------------------------------------
#ifndef __MY1GENEHPP__
#define __MY1GENEHPP__
//------------------------------------------------------------------------------
#include <vector>
using std::vector; // used by my1Genome
#include <list>
using std::list; // use by my1GenePool
//------------------------------------------------------------------------------
#define MY1GENE_VERSION "0.9.6"
#define MY1GENE_FILEID "[my1GenePool]"
#define MY1GENE_IDSIZE 14

#define DEFAULT_MUTATERATE 0.4
#define DEFAULT_DEATHRATE 0.4
#define DEFAULT_BIRTHRATE 0.4

#define IMPOSSIBLY_UNFIT -66.6
//------------------------------------------------------------------------------
// on 32-bit processors, sizeof(float) = sizeof(int) => HANDY!
// thus, sizeof(my1GeneData) = sizeof(int) = sizeof(float)!
// copy/transfer int = copy/transfer float!
union my1GeneData
{
	float mDecimal;
	int mInteger;
};
//------------------------------------------------------------------------------
enum my1GeneType
{
	GENE_INTEGER=0,GENE_FLOAT
};
//------------------------------------------------------------------------------
#define MY1GENE_DEF_TYPE GENE_INTEGER
#define MY1GENE_DEF_MIN 0
#define MY1GENE_DEF_MAX 1
//------------------------------------------------------------------------------
struct my1GeneInfo
{
	my1GeneType mType;
	my1GeneData mMin, mMax, mMid, mRange;
	my1GeneInfo()
	{
		mType = MY1GENE_DEF_TYPE;
		mMin.mInteger = MY1GENE_DEF_MIN;
		mMax.mInteger = MY1GENE_DEF_MAX;
		this->Calculate();
	}
	my1GeneInfo(my1GeneInfo& anInfo)
	{
		mType = anInfo.mType;
		mMin = anInfo.mMin;
		mMax = anInfo.mMax;
		mMid = anInfo.mMid;
		mRange = anInfo.mRange;
	}
	my1GeneInfo(my1GeneType aType,my1GeneData aMin,my1GeneData aMax)
	{
		this->SetType(aType);
		this->SetRange(aMin,aMax);
	}
	void SetType(my1GeneType aType)
	{
		mType = aType;
	}
	void SetRange(my1GeneData aMin,my1GeneData aMax)
	{
		mMin = aMin; mMax = aMax; // should be fine? like struct copy?
		this->Calculate();
	}
	void Calculate(void)
	{
		if(mType==GENE_FLOAT)
		{
			mRange.mDecimal = mMax.mDecimal - mMin.mDecimal;
			mMid.mDecimal = mMin.mDecimal + mRange.mDecimal/2;
		}
		else // if(mType==GENE_INTEGER) // this is default!
		{
			mRange.mInteger = mMax.mInteger - mMin.mInteger;
			mMid.mInteger = mMin.mInteger + mRange.mInteger/2;
		}
	}
};
//------------------------------------------------------------------------------
// should i use template instead of my1genedata?
class my1Gene
{
protected:
	my1GeneData mData;
	my1GeneInfo *mInfo;
	bool mLocalInfo; // internal flag
public:
	my1Gene(my1GeneInfo *pInfo);
	my1Gene(const my1Gene&);
	virtual ~my1Gene();
	my1GeneType Type(void);
	my1GeneData& Data(void);
	void Data(my1GeneData&);
	void Mutate(void);
	float Random(void);
};
//------------------------------------------------------------------------------
// phenotype - traits (visible) of organism
// genotype - genes within organism
class my1Genome // @chromosome - used to create individual!
{
protected:
	vector<my1Gene> mGenes;
	float mFitness;
public:
	my1Genome();
	my1Genome(const my1Genome&);
	~my1Genome();
	int GeneCount(void);
	my1Gene& GetGene(int);
	int InsertGene(int,const my1Gene&);
	int DeleteGene(int);
	float GetFitness(void);
	void SetFitness(float);
	void Crossover(const my1Genome&);
	void Mutation(void);
	// for sorting population
	bool operator<(const my1Genome&);
};
//------------------------------------------------------------------------------
class my1GenePool // population
{
protected:
	list<my1Genome> mGenomes;
	my1GeneInfo mInfo; // monolithic chromosome!
	int mEvoActive; // active evaluation index
	int mEvalIndex; // index - number of choromosomes evaluated
	int mGeneration; // generation index
	float mMutateRate;
	float mDeathRate;
	float mBirthRate;
	// only valid AFTER evolution - prev stat
	float mPrevBestFit;
	float mPrevAvgFit;
	float mPrevStdDev;
	int mPrevMutated;
public:
	my1GenePool();
	virtual ~my1GenePool();
	int PopulationSize(void);
	void KillPopulation(void); // resets everything
	void SortPopulation(void); // based on fitness
	my1Genome* GetGenome(int);
	int InsertGenome(int,my1Genome*);
	int DeleteGenome(int);
	my1GeneInfo* GetGeneInfo(void);
	// get evolution info
	int GetEvoActive(void);
	int GetEvalIndex(void);
	int GetGeneration(void);
protected:
	void ResetStatistic(void); // internal use only!
public:
	// set/get population rates
	float GetMutateRate(void);
	void SetMutateRate(float);
	float GetDeathRate(void);
	void SetDeathRate(float);
	float GetBirthRate(void);
	void SetBirthRate(float);
	// get prev population statistics- only valid after call to evolution
	float GetPrevBestFitness(void);
	float GetPrevAvgFitness(void);
	float GetPrevStdDeviation(void);
	int GetPrevMutated(void);
	// at any time - only evaluated samples are considered
	float GetBestFitness(int*);
	float GetAvgFitness(void);
	// evolution-related functions
	int NextEvalIndex(void); // called for each chromosome evaluation
	int DoneEvalIndex(void); // called after a chromosome has been updated
	void EvolutionReset(void); // no active, index = 0, maintain gen index
	bool EvolutionReady(void);
	virtual int Evolution(void);
	// file access
	int LoadFile(char* aFilename);
	int SaveFile(char* aFilename=0x0);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
