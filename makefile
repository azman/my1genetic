# makefile for my1genetic - implementation for genetic algorithm

PROJECT = my1genetic
MAINPRO = $(PROJECT)
MAINBIN = $(MAINPRO)
MAINOBJ = my1gene.o $(MAINPRO).o

DELETE = rm -rf
CFLAGS += -Wall --static
LFLAGS +=
OFLAGS +=
ifeq ($(OS),Windows_NT)
	MAINBIN = $(PROJECT).exe
	CFLAGS += -DDO_MINGW
	ifneq ($(shell echo),)
		DELETE = del
	endif
endif

CPP = g++
debug: CFLAGS += -DMY1DEBUG

all: main

main: $(MAINBIN)

new: clean all

debug: new

$(MAINBIN): $(MAINOBJ)
	$(CPP) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

%.o: src/%.cpp src/%.hpp
	$(CPP) $(CFLAGS) -c $<

%.o: src/%.cpp
	$(CPP) $(CFLAGS) -c $<

clean:
	-$(DELETE) $(MAINBIN) $(MAINOBJ) *.o
